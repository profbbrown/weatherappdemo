package com.example.sierra.weatherappdemo;

import android.widget.Toast;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by sierra on 4/20/15.
 */
public class Weather
{
    private JsonElement jse;
    private String location;

    public Weather(String location) {
        this.location = location;
    }

    public Conditions getConditions()
    {
        if (location.equals("") || location == null)
        {
            location="autoip";
        }

        try
        {
            String safeLocation = URLEncoder.encode(location, "utf-8");
            URL wuURL = new URL("http://api.wunderground.com/api/" + BuildConfig.ApiKey +
                    "/conditions/q/" + safeLocation + ".json");

            // Open connection
            InputStream is = wuURL.openStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            // Read the results into a JSON Element
            jse = new JsonParser().parse(br);

            // Close connection
            is.close();
            br.close();
        }
        catch (java.io.UnsupportedEncodingException uee)
        {
            uee.printStackTrace();
        }
        catch (java.net.MalformedURLException mue)
        {
            mue.printStackTrace();
        }
        catch (java.io.IOException ioe)
        {
            ioe.printStackTrace();
        }
        catch (java.lang.NullPointerException npe)
        {
            npe.printStackTrace();
        }

        //System.out.println(jse.toString());
        JsonObject current = jse.getAsJsonObject().get("current_observation").getAsJsonObject();
        String city = current.get("display_location").getAsJsonObject().get("full").getAsString();
        double temp = current.get("temp_f").getAsDouble();
        String weather = current.get("weather").getAsString();

        Conditions c = new Conditions(city, temp, weather);
        return c;
    }
}