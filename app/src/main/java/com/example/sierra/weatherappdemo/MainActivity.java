package com.example.sierra.weatherappdemo;

import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void fetchWeather(View v)
    {
        // Disable threading. We'll fix this later.
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy );


        EditText loc = findViewById(R.id.location);
        String theLocation = loc.getText().toString();

        Weather w = new Weather(theLocation);
        Conditions c = w.getConditions();

        TextView temp = findViewById(R.id.temp);
        temp.setText("" + c.getTemp());

        TextView city = findViewById(R.id.city);
        city.setText(c.getLocation());

        TextView weather = findViewById(R.id.weather);
        weather.setText(c.getWeather());
    }
}
