package com.example.sierra.weatherappdemo;

public class Conditions
{
    private String weather;
    private double tempF;
    private String location;

    public Conditions(String location, double tempF, String weather)
    {
        this.location = location;
        this.tempF = tempF;
        this.weather = weather;
    }

    public String getWeather()
    {
        return weather;
    }

    public double getTemp()
    {
        return tempF;
    }

    public String getLocation()
    {
        return location;
    }
}